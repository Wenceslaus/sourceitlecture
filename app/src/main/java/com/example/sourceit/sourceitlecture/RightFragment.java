package com.example.sourceit.sourceitlecture;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by wenceslaus on 01.10.16.
 */

public class RightFragment extends Fragment {

    public static RightFragment newInstance() {
        return new RightFragment();
    }

    private TextView label;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        label = (TextView) root.findViewById(R.id.fr_label);
        return root;
    }
}
